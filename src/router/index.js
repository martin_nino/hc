import Vue from 'vue'
import Router from 'vue-router'
import Assign from '@/components/Assign'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Assign',
      component: Assign
    }
  ]
})
